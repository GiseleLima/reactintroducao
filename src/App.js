import React from 'react';
import './App.css';
import profile from './assets/img1.png';

import TechList from './components/TechList';

function App() {
  return (
    <TechList />
  );
};

export default App;